const state = {
  marks: null,
  students: null,
  subjects: null,
  markreport: null,
  report: null,
}
const actions = {
  marks({ commit }, data) {
    return this.$axios.$get(`api/marks`).then((res) => {
      commit('SET_MAKS', res)
    })
  },
  marksCreate({ commit }, data) {
    return this.$axios.$post(`api/create/mark`, data)
  },
  students({ commit }, data) {
    return this.$axios.$get(`api/students`).then((res) => {
      commit('SET_STUDENTS', res)
    })
  },
  subjects({ commit }, data) {
    return this.$axios.$get(`api/subjects`).then((res) => {
      commit('SET_SUBJECTS', res)
    })
  },
  markReport({ commit }, id) {
    return this.$axios.$get(`api/student/${id}/mark/report`).then((res) => {
      commit('SET_MARK_REPORT', res)
    })
  },
  report({ commit }, id) {
    return this.$axios.$get(`api/group/subject/mark/report`).then((res) => {
      commit('SET_REPORT', res)
    })
  },
}
const getters = {}
const mutations = {
  SET_MAKS(state, val) {
    state.marks = val
  },
  SET_STUDENTS(state, students) {
    state.students = students
  },
  SET_SUBJECTS(state, subjects) {
    state.subjects = subjects
  },
  SET_MARK_REPORT(state, markreport) {
    state.markreport = markreport
  },
  SET_REPORT(state, report) {
    state.report = report
  },
}
export default {
  state,
  getters,
  actions,
  mutations,
}
